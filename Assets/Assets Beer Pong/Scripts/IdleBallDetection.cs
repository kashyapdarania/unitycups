﻿using UnityEngine;
using System.Collections;

public class IdleBallDetection : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.BEER_PONG) || other.CompareTag(Tags.DOUBLE_PONG))
        {
            Destroy(other.gameObject);
        }
    }
}
