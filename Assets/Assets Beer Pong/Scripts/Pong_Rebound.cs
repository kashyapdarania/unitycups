﻿using UnityEngine;
using System.Collections;

public class Pong_Rebound : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.BEER_PONG) && !other.CompareTag(Tags.DOUBLE_PONG))
        {
            other.tag = Tags.DOUBLE_PONG;
        }
    }
}
