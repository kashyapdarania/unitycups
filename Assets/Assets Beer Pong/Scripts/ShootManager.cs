﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ShootManager : MonoBehaviour
{
    [SerializeField]
    private ShootingPong Manager_Pong;
    [SerializeField]
    private GameObject Bar_force = default;
    [SerializeField]
    private Slider m_ForceSlider = default;

    void OnMouseUp()
    {
        m_ForceSlider.value = 0;
        Bar_force.gameObject.SetActive(false);
        Manager_Pong.Shoot();
    }

    void OnMouseDown()
    {
        Bar_force.gameObject.SetActive(true);
    }
}
