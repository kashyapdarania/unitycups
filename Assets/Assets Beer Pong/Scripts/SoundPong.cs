﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundPong : MonoBehaviour
{
    public float VOL_Pong;
    public float Vel;


    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 2)
        {
            var audioSource = GetComponent<AudioSource>();
            audioSource.Play();
            VOL_Pong -= 1;
            Vel += 0.1f;
            audioSource.volume -= VOL_Pong;
            if (audioSource.pitch < 2.8f)
            {
                audioSource.pitch += Vel;
            }
        }
    }
}
