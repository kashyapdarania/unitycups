﻿using Boo.Lang;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    PreStart,
    Started,
    Completed
}

public class ShootingPong : MonoBehaviour
{
    public static ShootingPong Instance;

    [SerializeField]
    private Rigidbody m_BallPrefab;
    public float Force = 10;
    public float MaxForce = 20;
    public int maxTime = 60;
    public int Time_update;
    public int Time_update_Max;
    public int ShootingThresold;
    public bool m_CanShoot;
    public Text scoreValue;
    public Text timeLabel;
    public Text Totalscore;

    private int _score;
    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            if (value == _score)
            {
                return;
            }

            _score = value;
            scoreValue.text = _score.ToString();
        }
    }

    public GameObject PopUpExit;
    public GameObject PopUpGameOver;
    public GameObject HUD;
    public bool isEndlessTime = false;
    [SerializeField]
    private Slider m_ForceSlider;
    [SerializeField]
    private Transform m_ForceBar;
    [SerializeField]
    private GameObject m_BallTarget;
    [SerializeField]
    private GameObject m_ShootingManager;

    [SerializeField]
    private Button m_BackButton;

    private GameState m_GameState;

    private List<string> m_TimeValues = new List<string>() { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10" };

    private int m_Minutes = 0, m_Seconds = 0;

    void Awake()
    {
        Instance = this;
        m_GameState = GameState.PreStart;

        // Check if is practice mode
        // tournamentID == 1 is practice
        string tournamentID = PlayerPrefs.GetString("tournamentID");
        if (tournamentID != "1")
        {
            // Is a challenge hidden the back button
            m_BackButton.gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        m_BackButton.onClick.AddListener(OnBackButtonClicked);
    }

    private void OnDisable()
    {
        m_BackButton.onClick.RemoveListener(OnBackButtonClicked);
    }

    private void OnBackButtonClicked()
    {
        MainMenu.Exit();
    }

    // Use this for initialization
    void Start()
    {
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        Time.timeScale = 1f;
        Time_update_Max = int.Parse(PlayerPrefs.GetString("updateTime"));
        Time_update = Time_update_Max;
        StartCoroutine(UpdateTimer());

        switch (PlayerPrefs.GetInt("game_mode"))
        {
            case 1:
                isEndlessTime = true;
                break;
            case 2:
                isEndlessTime = false;
                break;
        }

        if (isEndlessTime)
        {
            timeLabel.text = "00:00";
        }
        else
        {
            // add 11 to 59 to timeValues
            m_TimeValues.AddRange(Enumerable.Range(11, 49).Select(n => n.ToString()));

            StartCoroutine(HiddenObjectsTimer());
        }

        m_CanShoot = true;
        Score = 0;
        scoreValue.text = Score.ToString();

        m_GameState = GameState.Started;
    }

    public IEnumerator UpdateTimer()
    {
        ////Update_Time
        while (true)
        {
            while (Time_update > 0)
            {
                Time_update--;
                yield return new WaitForSeconds(1);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator HiddenObjectsTimer()
    {
        Time_update = Time_update_Max;

        m_Seconds = maxTime % 60;
        m_Minutes = maxTime / 60;

        var l_Delay = new WaitForSeconds(1f);

        while (true)
        {
            timeLabel.text = m_TimeValues[m_Minutes] + ":" + m_TimeValues[m_Seconds];

            if (m_Seconds == 0 && m_Minutes == 0)
            {
                break;
            }
            m_Seconds--;
            if (m_Seconds < 0)
            {
                m_Minutes--;
                m_Seconds = 59;
            }

            yield return l_Delay;
        }

        m_GameState = GameState.Completed;
        Time_update = 20;
        GameOver();
    }
    public void ResetShooting()
    {
        m_BallTarget.gameObject.SetActive(true);
        m_ShootingManager.gameObject.SetActive(true);
        m_CanShoot = true;
    }

    public void Shoot()
    {
        if (m_CanShoot)
        {
            m_CanShoot = false;

            m_BallTarget.gameObject.SetActive(false);
            m_ShootingManager.gameObject.SetActive(false);
            Rigidbody Ball_Pong = Instantiate(m_BallPrefab, m_BallTarget.transform.position, m_BallTarget.transform.rotation);
            Ball_Pong.AddRelativeForce(Ball_Pong.transform.forward * Force, ForceMode.VelocityChange);

            m_ForceSlider.value = 0;
            Invoke("ResetShooting", ShootingThresold);
        }
    }
    public void Pause()
    {
        Time.timeScale = 0;
        m_BallTarget.gameObject.SetActive(false);
        PopUpExit.gameObject.SetActive(true);
        HUD.gameObject.SetActive(false);
    }
    public void CancelPause()
    {
        Time.timeScale = 1;
        m_BallTarget.gameObject.SetActive(true);
        PopUpExit.gameObject.SetActive(false);
        HUD.gameObject.SetActive(true);
    }
    public void Exit()
    {
        //Clear Unused Textures
        Resources.UnloadUnusedAssets();

        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        PlayerPrefs.SetInt("game_mode", -1);
        SceneManager.LoadScene("menu");
    }

    public void GameOver()
    {
        m_BallTarget.gameObject.SetActive(false);
        Time.timeScale = 1;
        Totalscore.text = _score.ToString();
        PopUpGameOver.gameObject.SetActive(true);
        HUD.gameObject.SetActive(false);
        if (!isEndlessTime)
        {
#if !UNITY_EDITOR
            MainMenu.TotalPoints(Totalscore.text.ToString());
#endif
        }
    }

    public void TryAgain()
    {
        Time.timeScale = 1;
        PlayerPrefs.SetString("nextLevel", "Beer_pong");

        //Clear Unused Textures
        Resources.UnloadUnusedAssets();

        SceneManager.LoadScene("Beer_pong");
    }

    // Update is called once per frame
    void Update()
    {
        if (m_GameState != GameState.Started)
        {
            return;
        }

        m_ForceBar.transform.localScale = new Vector3(Force, 1.0f, 1.0f);
        Force = m_ForceSlider.value;

        if (Time_update == 0)
        {
#if !UNITY_EDITOR
				UpdatePoints();
#endif
        }

        if (Input.GetButtonDown("Jump"))
        {
            Shoot();
        }
    }
    public void UpdatePoints()
    {
        Time_update = int.Parse(PlayerPrefs.GetString("updateTime"));
#if UNITY_ANDROID || UNITY_IOS
        MainMenu.UpdatePoints(Score.ToString());
#endif
        //Debug.Log("actualizo puntos");
    }
}
