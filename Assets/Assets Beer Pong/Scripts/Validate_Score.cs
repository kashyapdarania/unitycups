﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Validate_Score : MonoBehaviour
{

    public float Time_max_efect;
    public int Total_Score;
    public Transform Point_Iniciate;
    public GameObject Particles;
    public GameObject Points;
    public GameObject Bonus;

    private AudioSource m_AudioSource;
    private Transform m_MainCameraTransform;
    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_MainCameraTransform = Camera.main.transform;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.BEER_PONG))
        {
            Destroy(other.gameObject);
            Invoke("ScorePong", 0);
        }
        if (other.CompareTag(Tags.DOUBLE_PONG))
        {
            Destroy(other.gameObject);
            Invoke("ScorePongDoble", 0);
        }
    }

    public void ScorePong()
    {
        Instantiate(Particles, Point_Iniciate.position, m_MainCameraTransform.rotation);
        Instantiate(Points, Point_Iniciate.position, m_MainCameraTransform.rotation);
        ShootingPong.Instance.Score += Total_Score;

        m_AudioSource.Play();
    }
    public void ScorePongDoble()
    {
        Instantiate(Particles, Point_Iniciate.position, m_MainCameraTransform.rotation);
        Instantiate(Bonus, Point_Iniciate.position, m_MainCameraTransform.rotation);
        ShootingPong.Instance.Score += Total_Score * 2;

        m_AudioSource.Play();

    }
}
