﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;


public class CameraBeerPong : MonoBehaviour
{
    private const float FORCE_DIRECTION_MULTIPLIER = 3f;
    public RectTransform container;

    public Transform m_ForceDirection;

    public float turnSpeed = 50f;
    public float RotationY;
    public float MinX;
    public float MaxX;
    private float RotationX;
    public float dragSpeed = 2;

    private Vector3 m_Position = Vector3.zero;
    private Vector3 m_PositionDifference;
    private Vector3 dragOrigin;
    private Camera m_MainCamera;


    private bool m_IsMouseDown = false;

    private void Start()
    {
        m_MainCamera = Camera.main;
    }

    private void OnEnable()
    {
        m_IsMouseDown = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(container, Input.mousePosition, m_MainCamera))
            {
                dragOrigin = Input.mousePosition;
                m_IsMouseDown = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            m_IsMouseDown = false;
            m_PositionDifference = new Vector3(0, 0, 0);
        }

        if (m_IsMouseDown)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(container, Input.mousePosition, m_MainCamera))
            {
                m_Position = m_MainCamera.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
                m_PositionDifference = new Vector3(m_Position.x * dragSpeed, 0, 0);
            }

            RotationX = Mathf.Clamp(m_PositionDifference.x, MinX, MaxX);
            transform.localEulerAngles = new Vector3(RotationY, RotationX, 0);
            m_ForceDirection.localEulerAngles = new Vector3(RotationY, RotationX * FORCE_DIRECTION_MULTIPLIER, 0);
        }
    }
}
