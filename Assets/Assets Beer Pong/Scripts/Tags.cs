﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags
{
    public const string BEER_PONG = "Beer_pong";
    public const string DOUBLE_PONG = "Doble_Pong";
}
