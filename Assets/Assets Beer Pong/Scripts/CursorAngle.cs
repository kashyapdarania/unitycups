﻿using UnityEngine;
using System.Collections;

public class CursorAngle : MonoBehaviour {
    public float turnSpeed = 50f;
    public float RotationY;
    public float MinX;
    public float MaxX;
    public float RotationX;
    //  public Joystick _Joystick;
    public Vector3 pos = Vector3.zero;
    public Vector3 move;
    public Vector3 moveY;
    public float dragSpeed = 2;
    private Vector3 dragOrigin;
    private Vector3 dragOriginY;
    // Use this for initialization
    public GameObject container;
    private Camera m_MainCamera;

    void Start()
    {
        m_MainCamera = Camera.main;
    }

    void Update()
    {
        RotationX = move.x;

        if (Input.GetMouseButtonDown(0))
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(container.GetComponent<RectTransform>(), Input.mousePosition, Camera.main))
            {
                dragOrigin = Input.mousePosition;
                return;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            move = new Vector3(0, 0, 0);
        }

        if (!Input.GetMouseButton(0)) return;
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(container.GetComponent<RectTransform>(), Input.mousePosition, Camera.main))
            {
                pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
                move = new Vector3(pos.x * dragSpeed, 0, 0);
            }

        }

        RotationX += 0 * Time.deltaTime;
        RotationX = Mathf.Clamp(RotationX, MinX, MaxX);
        transform.localEulerAngles = new Vector3(RotationY, RotationX*3, 0);
    }
}

