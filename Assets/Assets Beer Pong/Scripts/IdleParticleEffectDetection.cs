﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleParticleEffectDetection : MonoBehaviour
{
    public float m_DestroyAfter = 4f;

    private void Start()
    {
        Destroy(this.gameObject, m_DestroyAfter);
    }
}
